
![Capture.PNG](https://bitbucket.org/repo/XqbLXB/images/603728172-Capture.PNG)

## **Déploiement du projet** 

* 1) composer install
* 2) Créer une base de donnée symfony et configurer les paramètres du serveur local sur app/config/parameters.yml
* 2) Création d'un Virtual Host "festivalSymfony" pour pouvoir accéder aux différents liens du site
* 3) Création d'un user SUPER_ADMIN 
* 4) Cliquer sur "C'est partie" dans le back-office pour remplir la base de données

## Routes utiles : 

* / -> home page du site 
* /login -> Connection au back office 
* /admin -> Back office 
* /register -> Creer un utilisateur (Seulement pour les admins)

## principe des CRUD film

* /admin/film/show/id -> Affichage d'un film 
* /admin/film/list -> Affichage listes des films
* /admin/film/edit/id -> Editer un film


## principe des CRUD seance/cinema

* /admin/seance -> Affichage de la liste 
* /admin/seance/id -> Affichage le détail
* /admin/seance/id/edit -> Affichage de l'édit
* /admin/seance/id/delete -> Affichage du delete


## Commentaires : 

* On peut CRUD des séances, films, cinemas 
* Créer des exploitants et leurs assigner un cinema
* On ne peut pas lier des séances ou des couleurs au cinema et des acteurs au films

# Développeur(s) 
* Ismaël Sebbane 
* Céline Chamiot-Poncet