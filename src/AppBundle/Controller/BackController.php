<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cinema;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Film;
use AppBundle\Entity\Seance;

class BackController extends Controller
{
    /**
     * @Route("/admin", name="adminMain")
     */
    public function indexAction()
    {
        $isAdmin = $this->isUserAdmin();
        $em = $this->getDoctrine()->getManager();
        $films = $em->getRepository('AppBundle:Film')->findAll();
        $seances = $em->getRepository('AppBundle:Seance')->findAll();
        $data = array(
            'isAdmin'=> $isAdmin,
            'isDbEmpty'=>false,
            "films"=>$films
        );
        if($isAdmin) {
            if(count($films)==0 || count($seances)==0) {
                $data['isDbEmpty'] = true;
            }
            $data['managers'] = $this->getManagers();
        } else {
            $data['manager'] =  $this->get('security.token_storage')->getToken()->getUser();
        }
        return $this->render('AppBundle:Back:index.html.twig',$data);

    }

    public function isUserAdmin($roles = false) {
        $adminRole = "ROLE_SUPER_ADMIN";
        /* If no parameter given, i assign the value of $roles.
           but I can't use $this directly in the parameters */
        if(!$roles) {
            $roles = $this->getUser()->getRoles();
        }
        $isAdmin = in_array($adminRole,$roles) ? true : false;
        return $isAdmin;
    }
    public function getManagers() {
        $em = $this->getDoctrine()->getManager();
        $managers = $em->getRepository('AppBundle:User')->findAll();
        $managersLength = count($managers);
        $roles = null;
        // Yes, it could be dirty to do that this way. but anyway all users unless the admin have to be fetched there.
        for ($i=0; $i < $managersLength;$i++) {
            $roles = $managers[$i]->getRoles();
            if($this->isUserAdmin($roles)) {
                unset($managers[$i]);
            } else {
                $managedCinemas = count($managers[$i]->getCinemas());
                if($managedCinemas==1 && $managedCinemas > 0) {
                    $managers[$i]->hasOneCinema = true;
                    $managers[$i]->hasManyCinemas = false;
                } elseif($managedCinemas > 1) {
                    $managers[$i]->hasOneCinema = false;
                    $managers[$i]->hasManyCinemas = true;
                }
            }
        }
        return $managers;
    }
    // WARN: This func need at least 2 managers to work.
    /**
     * @Route("/admin/fillback",name="fillback")
     * @Method({"GET", "POST"})
     */
    public function fillBack() {
        $managers = $this->getManagers();
        $em = $this->getDoctrine()->getManager();
        $seances = $em->getRepository('AppBundle:Seance')->findAll();
        for($i=0;$i<2;$i++) {
            $cinema = new Cinema();
            $cinema->setName('Cinema'.$i);
            $cinema->setAddress("5 rue du mimosa");
            $cinema->setMail("cinema@gmail.com");
            $cinema->setWebsite("cinema.com");
            $cinema->setColor("dodgerblue");
            $cinema->addSeance($seances[$i+1]);
            // I fetch a 1 based index array with doctrine, wtf
            $cinema->setManager($managers[$i+1]);
            $em->persist($cinema);
        }
        $em->flush();
    }
    /**
     * @Route("/admin/cinema/{id}",name="cinemaAdmin",requirements={"id" = "\d+"})
     */
    public function showCinemaAction($id) {
        $data = [];
        $em = $this->getDoctrine()->getManager();
        $cinema = $em->getRepository('AppBundle:Cinema')->findById($id);
        $films = $em->getRepository('AppBundle:Film')->findAll();
        $data['cinema'] = $cinema[0];
        $data['films'] = $films;
//        var_dump($films);
        return $this->render('AppBundle:Back:cinemaView.html.twig',$data);
    }

    public function getFakeFilm($i) {
        $film = new Film();
        $list = "Le Royaume des chats,Le Château ambulant,Les Contes de Terremer,Ponyo sur la falaise, Arrietty le petit monde des chapardeurs, La Colline aux coquelicots,Le vent se lève, Le Conte de la princesse Kaguya, Souvenirs de Marnie,La Tortue rouge";
        $films = explode(",",$list);
        $film->setName($films[$i]);
        $film->setSynopsis("Idk");
        $film->setCover("https://media.senscritique.com/media/000006097952/source_big/Princesse_Mononoke.jpg");
        $film->setDuration(65);
        $film->setFilmMaker("Hayao Miazaki");
        $film->setReview('idk');
        $film->setSource("Japan");
        return $film;
    }

    /**
     * @Route("/admin/cinema/addseance")
     * @Method("POST")
     */
    public function addSeanceToCinema(Request $request) {
        var_dump($request);
    }
    /**
     * @Route("/admin/fillfront",name="fillfront")
     * @Method({"GET", "POST"})
     */
    public function fillFront() {
        // admin is under middleware but still, to get one more safety.
        if(!$this->isUserAdmin()) {
            return $this->redirectToRoute('adminMain');
        }
        $em = $this->getDoctrine()->getManager();
        for ($i=0;$i < 10;$i++) {
            $film = $this->getFakeFilm($i);
            $hour = $i.":30";
            $seance = new Seance($film,date('Y-m-d'),$hour);
            $em->persist($seance);
            $em->persist($film);
        }
        $em->flush();
        return $this->redirectToRoute('adminMain');
    }
}
