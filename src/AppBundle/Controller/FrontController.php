<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Film;
use AppBundle\Entity\Seance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\VarDumper\VarDumper;

class FrontController extends Controller
{
    /**
     * @Route("/",name="home")
     */
    public function indexAction() {
        return $this->render('AppBundle:FrontController:index.html.twig',array(
            "seances"=>$this->getSeancesOfToday()
        ));
    }

    /**
     * Utils functions for index.
     */
    public function getSeancesOfToday() {
        $em = $this->getDoctrine()->getManager();
        $today = date('Y-m-d');
        // Creating a date object set on the date of today. Backslash is here to use DateTime of global namespace.
        $date = new \DateTime($today);
        $condition = array("date"=>$date);
        $seances = $em->getRepository('AppBundle:Seance')->findBy($condition);
        $length = count($seances);
        // This loop defines the property filmId to the screenings so I can give it to the links in the list of films on the homepage.
        for ($i=0;$i < $length; $i++) {
            $seances[$i]->filmId = $seances[$i]->getFilm()->getId();
        }
        return $seances;
    }

    /**
     * @Route("/film/{id}",name="film",requirements={"id" = "\d+"})
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();
        $film = $em->getRepository('AppBundle:Film')->findOneById($id);
        $duration = $film->getDuration();
        if ($duration > 60) {
            $minutes = $duration % 60;
            $hours = ($duration - $minutes) / 60;
            if($hours==1) {
                $hoursString = "heure";
            } else if($hours>1) {
                $hoursString = "heures";
            }
            $film->setDuration($hours." ".$hoursString." ".$minutes." minutes");
        } else if($duration < 60){
            $film->setDuration($duration." minutes");
        }
        return $this->render("AppBundle:FrontController:filmView.html.twig",array(
            "film"=>$film,
            "seances"=>$this->getFilmScreenings($film)
        ));
    }
    public function getFilmScreenings(Film $film) {
        $em = $this->getDoctrine()->getManager();
        $condition = array("filmName"=>$film->getName());
        $seances = $em->getRepository('AppBundle:Seance')->findBy($condition);
        return $seances;
    }
}

