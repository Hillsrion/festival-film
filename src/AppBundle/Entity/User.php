<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Cinema",mappedBy="manager")
     */
    private $cinemas;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->cinemas = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getCinemas()
    {
        return $this->cinemas;
    }

    /**
     * @param mixed $cinemas
     */
    public function setCinemas($cinemas)
    {
        $this->cinemas = $cinemas;
    }

}