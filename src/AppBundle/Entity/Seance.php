<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Seance
 *
 * @ORM\Table(name="seance")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SeanceRepository")
 */
class Seance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="filmName",type="string", length=100)
     */
    private $filmName;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Film")
     * This annotation means that we cannot create a Seance without Film.
     * And that when we delete a film, it deletes this binded entity aswell.
     * @ORM\JoinColumn(nullable=false,name="film_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $film;

    /**
     * @ORM\Column(name="date",type="date")
     */
    private $date;
    /**
     * @ORM\Column(name="hour",type="string", length=5)
     */
    private $hour;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cinema")
     * This annotation means that we cannot create a Cinema without Film.
     */
    private $cinema;


    public function __construct(Film $film,$date,$hour)
    {
        $this->film = $film;
        $this->filmName = $film->getName();
        // On utilise le backslash pour pouvoir utiliser la classe Datetime du namespace global au lieu de celui de Symfo
        $this->date = new \DateTime($date);
        $this->hour = $hour;
        $this->setRandomId();
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getCinema()
    {
        return $this->cinema;
    }

    /**
     * @param mixed $cinema
     */
    public function setCinema($cinema)
    {
        $this->cinema = $cinema;
    }


    /**
     * @return mixed
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * @param mixed $hour
     */
    public function setHour($hour)
    {
        $this->hour = $hour;
    }


    /**
     * @return mixed
     */
    public function getSeanceId()
    {
        return $this->seanceId;
    }

    /**
     * @param mixed $seanceId
     */
    public function setSeanceId($seanceId)
    {
        $this->seanceId = $seanceId;
    }

    /**
     * @return Film instance
     */
    public function getFilm()
    {
        return $this->film;
    }

    public function setFilm(Film $film)
    {
        $this->film = film;
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getFilmName()
    {
        return $this->filmName;
    }

    public function setRandomId()
    {
        $random = uniqid();
        $this->filmId = $random;
    }

}

