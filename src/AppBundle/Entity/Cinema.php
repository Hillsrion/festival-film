<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Cinema
 *
 * @ORM\Table(name="cinema")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CinemaRepository")
 */
class Cinema
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",inversedBy="cinemas")
     * This annotation means that we cannot create a Cinema without a owner (in this case, a user).
     * @ORM\JoinColumn(nullable=false,name="manager_id", referencedColumnName="id")
     */
    private $manager;
    /**
     * @var string
     * @ORM\Column(name="name", type="string",length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="address", type="string",length=255)
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(name="mail", type="string",length=255)
     */
    private $mail;

    /**
     * @var string
     * @ORM\Column(name="website", type="string",length=255)
     */
    private $website;

    /**
     * @var string
     * @ORM\Column(name="color", type="string",length=20)
     */
    private $color;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Seance",mappedBy="cinemas")
     * */
    private $seances;


    public function __construct()
    {
        $this->seances = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User $manager
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param User $manager
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getSeances()
    {
        return $this->seances;
    }

    /**
     * @param mixed $seances
     */
    public function setSeances($seances)
    {
        $this->seances = $seances;
    }

    public function addSeance(Seance $seance) {
        $this->seances[] = $seance;
    }

}

