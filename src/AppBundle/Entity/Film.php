<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Film
 *
 * @ORM\Table(name="film")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FilmRepository")
 */
class Film
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string",length=100)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(name="review", type="string")
     */

    private $review;

    /**
     * @var string
     * @ORM\Column(name="filmMaker", type="string",length=100)
     */

    private $filmMaker;

    /**
     * @var string
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var string
     * Don't specify length get the column typed as longtext.
     * @ORM\Column(name="synopsis", type="string")
     */
    private $synopsis;

    /**
     * @var string
     * @ORM\Column(name="cover", type="string",length=255)
     */
    private $cover;

    /**
     * @var string
     * @ORM\Column(name="source", type="string",length=255)
     */
    private $source;

    /**
     * Get id
     * @return int
     */

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Seance",mappedBy="film")
     * */
    private $seances;

    public function __construct()
    {
        $this->seances = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFilmMaker()
    {
        return $this->filmMaker;
    }

    /**
     * @param string $filmMaker
     */
    public function setFilmMaker($filmMaker)
    {
        $this->filmMaker = $filmMaker;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * @param string $synopsis
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;
    }

    /**
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param string $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param string $review
     */
    public function setReview($review)
    {
        $this->review = $review;
    }

    /**
     * @return mixed
     */
    public function getSeances()
    {
        return $this->seances;
    }
    
}

