# Commandes utiles 
## Pour update la database
- php bin/console doctrine:schema:update --force
## Pour clear le cache
- php bin/console cache:clear --env=dev
## Pour promote un user
- php bin/console fos:user:promote username ROLE_ADMIN